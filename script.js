




  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
 
 
   const gORDER_COL = ["orderCode","kichCo","loaiPizza","idLoaiNuocUong","thanhTien","hoTen","soDienThoai","trangThai","chitiet"]
var gDataUser = "";
var gUserPut = []
var gId =[];
var gComboMenu = [];
var gPizza = [];
var gCodeVoucher = []
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  $(document).ready(function(){
    onPageLoading();
    $("#user-table").on("click",".edit-voucher",function() {
      var vOjbUserOrder = getDataUserFromButton(this)
      gId = vOjbUserOrder.id
         loadDataUserModal(vOjbUserOrder)
        $("#my-modal").modal("show");
    });
    $("#user-table").on("click",".delete-voucher",function() {
      var vOjbUserOrder = getDataUserFromButton(this)
      gId = vOjbUserOrder.id;
        $("#modal-delete").modal("show");
    });
    //event click filter User
    $("#btn-fliter").click(function() {
      onBtnFilterUserClick()
    });
    //event click Confirmed
    $("#btn-confirm").click(function() {
      onBtnComfirmClick()
    });
    $("#btn-cancel").click(function() {
      onBtnCancleClick()
    });
    // event click btn them 
    $("#btn-them").click(function(){
      console.log("test")
      onBntClickThem() 
    });
    // event on change select comboMenu
  $("#int-combo").on("change", function() {
    onComboSelectChange(this);
  });
   // event on change select comboMenu
   $("#int-Pizza-post").on("change", function() {
    onPizzaSelectChange(this);
  });
  $("#int-vuocher-post").on("change",function() {
    onVouchertChange(this)
  })
  //event click create order
  $("#btn-create-order").click(function(){
    onBtnCreateOrderClick()
  });
  //event cancle create order
  $("#btn-cancel-order").click(function(){
    trimFormReateOrder()
  })
  //event click delete order
  $("#btn-delete-modal").click(function() {
    onBtnDeleteClick()
  })
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    //onload
    function onPageLoading() {
        getApiOrderUser()
        loadDataTableUser(gDataUser)
        //
        getDataApiDrink();
    };
    //on click btn create order
    function  onBtnCreateOrderClick() {
      var vOrderUser = getDataOrderUser()
      console.log(vOrderUser);
      var vCheck = checkValidatedForm(vOrderUser);
      if(vCheck ==true){
        postCreateOrder(vOrderUser)
        // B3 load data table
      loadDataTableUser(gDataUser);
      $("#my-modal-create-Order").modal("hide");
      trimFormReateOrder()
      }
    
    }
  // on click btn them 
  function onBntClickThem(){
    $("#my-modal-create-Order").modal("show");
  }
 //this is event click btn Comfim
 function onBtnComfirmClick() {

  var vObjStatus = {
       trangThai : "confirmed"
  }
  if(gId !=""){
    putStatusOrderList(vObjStatus)
    // B3 load data table
    loadDataTableUser(gDataUser);
   $("#my-modal").modal("hide");
  }
};
//this is event click btn 
function onBtnCancleClick() {
  var vObjStatus = {
       trangThai : "cancle"
  }
  if(gId !=""){
    putStatusOrderList(vObjStatus)
    // B3 load data table
    loadDataTableUser(gDataUser);
   $("#my-modal").modal("hide");
  }
};
//this is event click btn delete
function onBtnDeleteClick() {
  if(gId !=""){
    deleteOrderUser(gId)
    // B3 load data table
    loadDataTableUser(gDataUser);
   $("#modal-delete").modal("hide");
  }
};
 
//this is event click btn Fliter user
function onBtnFilterUserClick(){
  // khai báo đối tượng để chứa dữ liệu trên form
  var vFilterObj = {
    trangThai: "",
    loaiPizza: ""
  };
  // B1 thu thập dữ liệu
  getFilterFormData(vFilterObj);
  console.log(vFilterObj)
  // B2 xử lý lọc dữ liệu.
  var vUsersResult = filterUsers(vFilterObj);
  console.log(vUsersResult)
  // B3 Hiển thị
  loadDataTableUser(vUsersResult);
  //B4 xóa trắng bảng
  trimInpSelectOrderLoaiPizza()
};
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //load data table
  function dataTable() {
    var vTable = $("#user-table").DataTable({
        columns:[
            {data:gORDER_COL[0]},
            {data:gORDER_COL[1]},
            {data:gORDER_COL[2]},
            {data:gORDER_COL[3]},
            {data:gORDER_COL[4]},
            {data:gORDER_COL[5]},
            {data:gORDER_COL[6]},
            {data:gORDER_COL[7]},
            {data:gORDER_COL[8]},
        ],
        columnDefs:[
              {targets:[8],
               className:"text-center",
               defaultContent:`  <img class="edit-voucher" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                                 <img class="delete-voucher" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                              `
            }                
          ]   
    })
    return vTable
  };
  //put api status orderrlist
  function putStatusOrderList(paramStatus) {
    var vArrayIndex = getArrayUserById(gId);
    const baseUrl = "http://203.171.20.210:8080/devcamp-pizza365/orders/"  + gId;
    $.ajax({
      url: baseUrl,
      async:false,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramStatus),
      //B4: xử lý hiển thị
      success: function(res){
        console.log(res)
          gDataUser.splice(vArrayIndex,1, res);
          console.log("id User " + res.id)
      },
      error: function(ajaxContext){
          alert (ajaxContext.responseText);
      }
  });
  };
  
  // get api json Order User
  function getApiOrderUser() {
     var vBaseUrl = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    $.ajax({
        url: vBaseUrl,
        async:false,
        type: "GET",
        success:function(data){
            //console.log(data)
            gDataUser = data;
            console.log(gDataUser)
        },
        error: function(ajaxContext){
            alert(ajaxContext.responseText)
        }
      })
    };
    //load data table
function loadDataTableUser(paramOrderUserArray){
    var vDataTable = dataTable()
    vDataTable.clear();
    vDataTable.rows.add(paramOrderUserArray);
    vDataTable.draw();
 };
  // this is get data userOrder by button 
function getDataUserFromButton(paramElement) {
    var vRowClick = $(paramElement).closest("tr"); // Xác định tr chứa nút bấm được click
    var vTable = $('#user-table').DataTable(); // tạo biện truy xuất đến datatable
    var vDataRow = vTable.row( vRowClick ).data(); // Lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow)
    return vDataRow
  };
   // hàm thu thập dữ liệu lọc trên form
   function getFilterFormData(paramFilterObj) {
    paramFilterObj.trangThai = $("#inp-status-order-fillter").val();
     paramFilterObj.loaiPizza = $("#inp-pizza-fillter").val();
  };
   // hàm thực hiện lọc users trong mảng, và trả về mảng users sau khi lọc thỏa mãn điều kiện lọc
  function filterUsers(paramFilterObj) {
    var vOrders = gDataUser;
   
    var vOrder = gDataUser.filter((paramOrder, index)=>{

      return paramOrder.loaiPizza &&(paramFilterObj.trangThai === "all" || paramFilterObj.trangThai.toUpperCase() === paramOrder.trangThai.toUpperCase())
            && (paramFilterObj.loaiPizza === "all" || paramFilterObj.loaiPizza.toUpperCase() === paramOrder.loaiPizza.toUpperCase());
      
    })
    console.log(vOrders)

    return vOrder;
}
  // get array by id
function getArrayUserById(paramTypeId) {
  "use strict";
  var vNewIndex = "";
  var vIndex = 0;
  var vIsTypeFound = false;
  while(!vIsTypeFound && vIndex < gDataUser.length) {
    if(gDataUser[vIndex].id === paramTypeId) {
      vIsTypeFound = true;
      vNewIndex = vIndex;
    }
    else {
      vIndex ++;
    }
  }
  return vNewIndex;
}
  // hàm xóa bỏ dấu tiếng Việt
  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g," ");
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    return str;
};
// on load data user in form modal 
function loadDataUserModal(paramDataUser) {
   $("#inp-orderid").val(paramDataUser.id);
   $("#select-combo").val(paramDataUser.kichCo).change();
   $("#inp-duongkinh").val(paramDataUser.duongKinh);
   $("#inp-suonnuong").val(paramDataUser.suon);
   $("#select-drink").val(paramDataUser.idLoaiNuocUong);
   $("#inp-numberdrink").val(paramDataUser.soLuongNuoc);
   $("#inp-vuocher").val(paramDataUser.idVourcher);
   $("#select-pizza").val(paramDataUser.loaiPizza);
   $("#inp-salad").val(paramDataUser.salad);
   $("#inp-price").val(paramDataUser.thanhTien);
   $("#inp-discount").val(paramDataUser.giamGia);
   $("#inp-fullname").val(paramDataUser.hoTen);
   $("#inp-email").val(paramDataUser.email);
   $("#inp-numberphone").val(paramDataUser.soDienThoai);
   $("#inp-address").val(paramDataUser.diaChi);
   $("#inp-message").val(paramDataUser.loiNhan);
   $("#inp-status").val(paramDataUser.trangThai);
   $("#inp-datecreated").val(paramDataUser.ngayCapNhat);
   $("#inp-updateday").val(paramDataUser.ngayTao);
};
//get api data Drink
function getDataApiDrink() {
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
    async:false,
    type: "GET",
    success:function(data){
      loadSelectDrink(data)
      console.log(data)
      },
    error: function(ajaxContext){
        alert(ajaxContext.responseText)
    }
});
};
// hàm xử lý sự kiện select cuontry change
function onComboSelectChange(paramComboSize) {
  // B1: thu thập dữ liệu
  var vCombosize = $(paramComboSize).val()
  //
  changeComboSizeByComboMenu(vCombosize);
  //
  var ObjUser = getDataOrderUser();
  loadMenuComboBySize(ObjUser)
}
// hàm xử lý sự kiện select Piza change
function onVouchertChange(paramVoucherCode) {
  // B1: thu thập dữ liệu
     gCodeVoucher = $(paramVoucherCode).val();
  if(gComboMenu !=""){
    var ObjUser = getDataOrderUser();
    loadMenuComboBySize(ObjUser)
  }
}
// hàm xử lý sự kiện select Piza change
function onPizzaSelectChange(paramPizza) {
  // B1: thu thập dữ liệu
    gPizza = $(paramPizza).val();
  console.log(gPizza )
  //
  
}
//this is index combo menu
function getComboSelected(
  paramDuongKinhCM,
  paramSuongNuong,
  paramSaladGr,
  paramDrink,
  paramPriceVND
) {
  var vSelectedMenu = {
    duongKinhCM: paramDuongKinhCM,
    suongNuong: paramSuongNuong,
    saladGr: paramSaladGr,
    drink: paramDrink,
    priceVND: paramPriceVND,
    displayInConsoleLog() {
      console.log("%cPLAN MENU COMBO - .........", "color:blue");
      console.log("duongKinhCM: " + this.duongKinhCM);
      console.log("suongNuong: " + this.suongNuong);
      console.log("saladGr: " + this.saladGr);
      console.log("drink:" + this.drink);
      console.log("priceVND: " + this.priceVND);
    },
  };
  return vSelectedMenu;
};
//this is change combo size by combomenu
function changeComboSizeByComboMenu(paramComboSize) {
  if(paramComboSize == "S"){
    gComboMenu = getComboSelected( 20, 2, 200, 2, 150000);
    console.log("test")
  }
  if(paramComboSize == "M"){
    gComboMenu = getComboSelected(25, 4, 300, 3, 200000)
  }
  if(paramComboSize == "L"){
    gComboMenu = getComboSelected( 30, 8, 500, 4, 250000);
    
     }
    gComboMenu.displayInConsoleLog()
  }
 //this is load index select drink Pizza365
 function loadSelectDrink(paramDataDrink) {
  var vSelectDrink = $("#select-drink-postmodal")
  for(var i = 0; i <paramDataDrink.length ; i++){
   var vOption = $("<option>").text(paramDataDrink[i].tenNuocUong)
                              .val(paramDataDrink[i].maNuocUong)
   vOption.appendTo(vSelectDrink)
  }
 };
// get Api voucher 
function getApiVoucher(paramVoucherId) {
  var vResponse = null
    if(paramVoucherId ==""){
       vResponse = null
    }
    else{
      $.ajax({
        url:  "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucherId,
        async:false,
        type: "GET",
        success:function(data){
          loadSelectDrink(data)
          console.log(data)
          vResponse = data
          gCodeVoucher = data.maVoucher
          },
        error: function(ajaxContext){
            alert("Không có mã giảm giá")
        }
    });
    } 
    return vResponse
}
// create Order
function postCreateOrder(paramOrder) { 
  //base url
  const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";  
  var vObjUser =  {
    kichCo: paramOrder.comboSize ,
   duongKinh: paramOrder.menuCombo.duongKinhCM,
   suon: paramOrder.menuCombo.suongNuong,
   salad:paramOrder.menuCombo.saladGr,
   loaiPizza:  paramOrder.loaiPizza ,
   idVourcher:  paramOrder.voucher,
   idLoaiNuocUong: paramOrder.orderdrink,
   soLuongNuoc:  paramOrder.menuCombo.drink,
   hoTen: paramOrder.hoVaTen,
   thanhTien:paramOrder.priceAnnualVND(),
   email:paramOrder.email,
   soDienThoai:paramOrder.dienThoai,
   diaChi: paramOrder.diaChi,
   loiNhan: paramOrder.loiNhan
  };
  $.ajax({
    url:  vBASE_URL,
    async:false,
    type: 'POST',
    dataType: 'json', // added data type
    contentType: "application/json",
    data:JSON.stringify(vObjUser),
    success: function (responseObject) {
      //bước 4. xử lý hiện thị 
        alert("Post success full")
        var vArrayIndex = getArrayUserById(responseObject.id);
        gDataUser.splice(vArrayIndex,1,responseObject);
     console.log(responseObject)
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    }
  });
};
//delete order user
function deleteOrderUser(paramgId) {
  var vArrayIndex = getArrayUserById(gId);
  const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";  
  $.ajax({
    url:  vBASE_URL  +"/"+ paramgId,
    async:false,
    dataType: 'json', // added data type
    contentType: "application/json",
    type: 'DELETE',
    success: function(result) {   
     gDataUser.splice(vArrayIndex,1);
      alert ("success delete user")
    }
});
}
//get data infor Order user
function getDataOrderUser() {
  var vCombosize = $("#int-combo").val()
  var vName  = $("#int-fullname-post").val().trim();
  var vEmail = $("#int-email-post").val().trim();
  var vNumberPhone = $("#int-numberphone-post").val().trim();
  var vAddress = $("#int-address-post").val().trim();
  var vVoucher = gCodeVoucher;
  var vMessage = $("#int-message-post").val().trim();
  var vOrderdrink =  $("#select-drink-postmodal").val();
  var vPercent = 0;
  // goi ham tinh phan tram
  var vVoucherObj = getApiVoucher(gCodeVoucher);
  if (vVoucher != ""  && vVoucherObj != null) {
    vPercent = vVoucherObj.phanTramGiamGia;
  }
  var vOrderInfo = {
    comboSize:vCombosize,
    menuCombo: gComboMenu,
    loaiPizza: gPizza,
    hoVaTen: vName,
    email: vEmail,
    dienThoai: vNumberPhone,
    diaChi: vAddress,
    loiNhan: vMessage,
    voucher: vVoucher,
    orderdrink:vOrderdrink,
    phanTramGiamGia:function(){
      var vDiscount = 0 
      if(vPercent){
        vDiscount = vPercent
      }
      return vDiscount
      },
    giamGia:function(){
      var vgiamGia = 0
      vgiamGia = this.menuCombo.priceVND * ( vPercent / 100);
      return vgiamGia
    },
    priceAnnualVND: function () {
      var vTotal = this.menuCombo.priceVND
      if(vPercent){
        var vTotal = this.menuCombo.priceVND * (1 - vPercent / 100);
      }
      return vTotal;
    }
  };
  return vOrderInfo;
};
//load combo menu
function loadMenuComboBySize(paramOrder) {
  $("#int-discount-post").val(paramOrder.giamGia()+"Vnd")
  $("#int-price").val(paramOrder.priceAnnualVND()+"Vnd")
  $("#select-index-menu").html(
    ` 
    <option selected>${"Menu combo:"}</option>
    <option value="1">${"Kích cỡ:"}${paramOrder.comboSize},   ${"Đường kính:"}${paramOrder.menuCombo.duongKinhCM}${"Cm"}</option>
    <option value="2"> ${"Nước:"}${paramOrder.menuCombo.drink},  ${"Giá: "}${paramOrder.menuCombo.priceVND}${"vnd"},</option>
    <option value="3">${"Suờn nướng"}${paramOrder.menuCombo.suongNuong},  ${"SaladGr:"}${paramOrder.menuCombo.saladGr}${"gram"}</option>
    `
     )
};
// trim form create Ordder
function trimFormReateOrder() {
  gComboMenu =[]
  $("#int-vuocher-post").val("");
  $("#int-combo").val("");
  $("#select-drink-postmodal").val("");
  $("#int-fullname-post").val("");
  $("#select-index-menu").html("");
  $("#int-Pizza-post").val("");
  $("#int-discount-post").val("");
  $("#int-email-post").val("");
  $("#int-address-post").val("");
  $("#int-price").val("");
  $("#int-numberphone-post").val("");
  $("#int-message-post").val("");
};
// trim selct order loaiPzza
function trimInpSelectOrderLoaiPizza() {
  $("#inp-status-order-fillter").val("all");
  $("#inp-pizza-fillter").val("all");
};
//validate data user input
 // output: dung/ sai (true/ false)
 function checkValidatedForm(paramOrder) { 
  if (paramOrder.menuCombo == "") {
    alert("Chọn combo pizza...");
    return false;
  }
  if (paramOrder.loaiPizza == "") {
    alert("Chọn loại pizza...");
    return false;
  }
  if (paramOrder.orderdrink =="") {
    alert("Chọn loại đồ uống...");
    return false;
  }
  if (paramOrder.hoVaTen == "") {
    alert("Nhập họ và tên");
    return false;
  }
  if (isEmail(paramOrder.email) == false) {
    return false;
  }

  if (paramOrder.dienThoai == "") {
    alert("Nhập vào số điện thoại...");
    return false;
  }
  if (paramOrder.diaChi === "") {
    alert("Địa chỉ không để trống...");
    return false;
  }
  return true;
}
  //hàm kiểm tra email validate email 
  function isEmail(paramEmail) {
    if (paramEmail < 3) {
      alert("Nhập email...");
      return false;
    }
    if (paramEmail.indexOf("@") === -1) {
      alert("Email phải có ký tự @");
      return false;
    }
    if (paramEmail.startsWith("@") === true) {
      alert("Email không bắt đầu bằng @");
      return false;
    }
    if (paramEmail.endsWith("@") === true) {
      alert("Email kết thúc bằng @");
      return false;
    }
    return true;
  }

